<?php
function reportError($message) {
	 die ('{"status":"error" , "message":"' . $message . '"}');
}

//get the post variables
if(isset($_POST['name']) && !empty($_POST['name']))	$name=$_POST['name']; else	reportError('You have not entered your name');
if(isset($_POST['email']) && !empty($_POST['email'])) $email=$_POST['email']; else reportError('You have not entered your email');
if(isset($_POST['phone']) && !empty($_POST['phone'])) $phone=$_POST['phone']; else reportError('You have not entered your phone');
if(isset($_POST['location']) && !empty($_POST['location'])) $location=$_POST['location']; else reportError('You have not entered your location');
if(isset($_POST['pick_up_keys_location']) && !empty($_POST['pick_up_keys_location'])) $pick_up_keys_location=$_POST['pick_up_keys_location']; else reportError('You have not entered your pick_up_keys_location');
if(isset($_POST['service']) && !empty($_POST['service'])) $service=$_POST['service']; else reportError('You have not entered your service');
if(isset($_POST['subservice']) && !empty($_POST['subservice'])) $subservice=$_POST['subservice']; else reportError('You have not entered your subservice');
if(isset($_POST['datetime']) && !empty($_POST['datetime'])) $datetime=$_POST['datetime']; else reportError('You have not entered your datetime');
if(isset($_POST['number_of_cleaners']) && !empty($_POST['number_of_cleaners'])) $number_of_cleaners=$_POST['number_of_cleaners']; else reportError('You have not entered your number_of_cleaners');
if(isset($_POST['payment_method']) && !empty($_POST['payment_method'])) $payment_method=$_POST['payment_method']; else reportError('You have not entered your payment_method');
if(isset($_POST['comment'])) $comment=$_POST['comment']; else $comment='';

$location_name='name of location';
$pick_up_keys_location_name='name of location';

try{
//get the location names
$response = file_get_contents("https://maps.googleapis.com/maps/api/geocode/json?latlng=".$location."&key=AIzaSyCgWnE2Rp2oEKKiE5DguGIqSnJ8ryZQ7gQ");
$response_json = json_decode($response,true);
if($response_json['status']=='OK')
$location_name=$response_json["results"][0]["formatted_address"];
else $location_name=$location;


//get the picpup keys location name

$response = file_get_contents("https://maps.googleapis.com/maps/api/geocode/json?latlng=".$pick_up_keys_location."&key=AIzaSyCgWnE2Rp2oEKKiE5DguGIqSnJ8ryZQ7gQ");
$response_json = json_decode($response,true);
if($response_json['status']=='OK')
$pick_up_keys_location_name=$response_json["results"][0]["formatted_address"];
else $pick_up_keys_location_name=$location;

}
catch(Exception $ex)
{
	reportError("An error occured.".$ex->getMessage());
}



//send the message
// Be sure to include the file you've just downloaded
require_once('AfricasTalkingGateway.php');
// Specify your login credentials
$username   = "stankim";
$apikey     = "20a712b5a9c364d6011d3e13e3c38b0dba6ee3c2d694e268632659ab2609b74c";
// NOTE: If connecting to the sandbox, please use your sandbox login credentials
// Specify the numbers that you want to send to in a comma-separated list
// Please ensure you include the country code (+254 for Kenya in this case)
$recipients = "+254713166021";
// And of course we want our recipients to know this
$message    = $name. ' ' .$email .' '.$phone.' '.$location_name.' '.$pick_up_keys_location_name.' '. $service.' '.$subservice.' '.$datetime .' '.$number_of_cleaners .' '.$payment_method.' '. $comment;

// Create a new instance of our awesome gateway class
$gateway    = new AfricasTalkingGateway($username, $apikey);
// NOTE: If connecting to the sandbox, please add the sandbox flag to the constructor:
/*************************************************************************************
             ****SANDBOX****
$gateway    = new AfricasTalkingGateway($username, $apiKey, "sandbox");
**************************************************************************************/
// Any gateway error will be captured by our custom Exception class below, 
// so wrap the call in a try-catch block
try 
{ 
       // Thats it, hit send and we'll take care of the rest. 
       $results = $gateway->sendMessage($recipients, $message);

//  just incase you need these variables
  //foreach($results as $result) {
    // status is either "Success" or "error message" 
   // echo " Number: " .$result->number;
    // echo " Status: " .$result->status;
   // echo " MessageId: " .$result->messageId;
   // echo " Cost: "   .$result->cost."\n";
  //}
}
catch ( AfricasTalkingGatewayException $e )
{
  reportError("Encountered an error while sending: ".$e->getMessage());
}

//send email
//$to      = 'stankim.ks@gmail.com';
//$subject = 'Osha Order';
//$message = 'Hi, you have a new order. \n'. $message;
//$headers = 'From: webmaster@osha.co.ke' . "\r\n" .
 //   'Reply-To: webmaster@osha.co.ke' . "\r\n" .
//    'X-Mailer: PHP/' . phpversion();
//mail($to, $subject, $message, $headers);


//send response to client

echo '
{
"status":"success",
"details":{
	"name":"'.$name.'",
	"email":"'.$email.'",
	"phone":"'.$phone.'", 
	"location":"'.$location.'",
	"pick_up_keys_location":"'.$pick_up_keys_location.'",
	"service":"'.$service.'", 
	"subservice":"'.$subservice.'", 
	"datetime":"'.$datetime.'",
	"number_of_cleaners":"'.$number_of_cleaners.'",
	"payment_method":"'.$payment_method.'",
	"comment":"'.$comment.'"
		}
}
';
?>
